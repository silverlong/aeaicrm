package com.agileai.crm.module.mytasks.handler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.agileai.crm.cxmodule.ProCustomerSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class ProCustomerSelectListHandler
        extends PickFillModelHandler {
    public ProCustomerSelectListHandler() {
        super();
        this.serviceId = buildServiceId(ProCustomerSelect.class);
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		User user = (User) getUser();
		param.put("ORG_SALESMAN", user.getUserId());
		Date date = DateUtil.getDateAdd(new Date(), DateUtil.DAY, -7);
		String weekBeforeDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, date);
		param.put("weekBeforeDate", weekBeforeDate);
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		
		String taskFiltes = param.getString("taskFilte");
		
		List<DataRow> rsList = new ArrayList<DataRow>();
		
		if(taskFiltes !=null){
			String taskFilteArray[] = taskFiltes.split(",");
	    	for(int i=0;i < taskFilteArray.length;i++){
	        	String taskFilte = taskFilteArray[i];
	        	String sqlSyntax = " ";
	    		if("NOTFOR".equals(taskFilte)){
//	    			sqlSyntax = "and a.ORG_ID not in (SELECT DISTINCT ORG_ID FROM crm_my_tasks a " +
//	    					"LEFT JOIN crm_task_review b ON a.TASK_REVIEW_ID = b.TASK_REVIEW_ID WHERE 1 = 1 " +
//	    					"and a.TASK_CLASS = 'ColdCalls' and a.TASK_FOLLOW_STATE = 'HaveFollowUp')";
	    			
	    			sqlSyntax = " and not exists (SELECT ORG_ID FROM crm_procust_visit aa "
	    					+" where a.ORG_ID = aa.org_id "
	    					+" and aa.PROCUST_VISIT_FILL_ID = '"+user.getUserId()+"')";
	    			
	    		}else if("HISTORY".equals(taskFilte)){
	    			sqlSyntax = "and a.ORG_ID in (SELECT DISTINCT ORG_ID FROM crm_my_tasks a " +
	    					"LEFT JOIN crm_task_review b ON a.TASK_REVIEW_ID = b.TASK_REVIEW_ID WHERE 1 = 1 " +
	    					"and a.TASK_CLASS = 'ColdCalls' and a.TASK_FOLLOW_STATE = 'HaveFollowUp')";
	    		}else if("LASTWEEK".equals(taskFilte)){
	    			sqlSyntax = "and a.ORG_ID in (select ORG_ID from crm_my_tasks a left join crm_task_review b on a.TASK_REVIEW_ID = b.TASK_REVIEW_ID " +
	    					"where 1=1 and b.TC_ID = ( " +
	    					"select b.TC_ID from crm_task_review a left join crm_task_cycle b on a.TC_ID = b.TC_ID where 1=1 " +
	    					"and SALE_ID = '"+param.getString("ORG_SALESMAN")+"' "+
	    					"and '"+weekBeforeDate+"' >= b.TC_BEGIN " +
	    					"and '"+weekBeforeDate+"' <= b.TC_END " +
	    					"and a.TASK_FOLLOW_STATE = 'HaveFollowUp' " +
	    					"order by b.TC_BEGIN desc)) ";
	    		}
	    		param.put("sqlSyntax", sqlSyntax);
	    		List<DataRow> tempRsList = getService().queryPickFillRecords(param);
	    		rsList.addAll(tempRsList);
	    	}
		}else{
			rsList = getService().queryPickFillRecords(param);
		}

		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
    	setAttribute("taskFilte",
                FormSelectFactory.create("TASK_FILTE")
                                 .addSelectedValue(getAttributeValue("taskFilte",
                                                                          "NOTFOR,")));
    }

    protected void initParameters(DataParam param) {
    }
    
    protected ProCustomerSelect getService() {
        return (ProCustomerSelect) this.lookupService(this.getServiceId());
    }
}
