package com.agileai.crm.module.mytasks.exteral;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/rest")
public interface MyTasks {
	
    @GET  
    @Path("/work-check-list/")
    @Produces(MediaType.TEXT_PLAIN)
	public String findWorkCheckList();
    
    @GET  
    @Path("/potential-cust-list/{infoParam}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findPotentialCustList(@PathParam("infoParam") String infoParam);
    
    @GET  
    @Path("/create-intention-task/{taskReviewId}/{taskReviewState}/{orgIds}")
    @Produces(MediaType.TEXT_PLAIN)
	public String createStrangeTaskRecord(@PathParam("taskReviewId") String taskReviewId, @PathParam("taskReviewId") String taskReviewState, @PathParam("orgIds") String orgIds);
    
    @GET  
    @Path("/strange-list/{taskReviewId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findStrangeVisitList(@PathParam("taskReviewId") String taskReviewId);
    
    @GET
    @Path("/strange-info/{id}") 
    @Produces(MediaType.TEXT_PLAIN)
	public String getStrangeVisitInfo(@PathParam("id") String id);
    
    @GET
    @Path("/get-strange-visit-info/{id}") 
    @Produces(MediaType.TEXT_PLAIN)
	public String getStrangeVisitRecordInfo(@PathParam("id") String id);
    
    //findMyCustList
    @POST 
    @Path("/my-cust-list")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String findMyCustList(String infoParam);
    
    @GET  
    @Path("/create-strange-task/{taskReviewId}/{taskReviewState}/{custIds}")
    @Produces(MediaType.TEXT_PLAIN)
	public String createIntentionTaskRecord(@PathParam("taskReviewId") String taskReviewId, @PathParam("taskReviewId") String taskReviewState, @PathParam("custIds") String custIds);
    
    @GET  
    @Path("/intention-list/{taskReviewId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findIntentionFollowUpList(@PathParam("taskReviewId") String taskReviewId);
    
    @GET
    @Path("/intention-info/{id}") 
    @Produces(MediaType.TEXT_PLAIN)
	public String getIntentionFollowUpInfo(@PathParam("id") String id);
    
    @GET
    @Path("/get-intention-visit-info/{id}") 
    @Produces(MediaType.TEXT_PLAIN)
	public String getIntentionVisitRecordInfo(@PathParam("id") String id);
    
    @POST
    @Path("/suspend-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String suspendInfo(String info);
    
    @POST
    @Path("/followup-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String followUpInfo(String info);
    
    @GET
    @Path("/get-summary-info/{taskReviewId}") 
    @Produces(MediaType.TEXT_PLAIN)
	public String getSummaryInfo(@PathParam("taskReviewId") String taskReviewId);
    
    @GET
    @Path("/save-summary-info/{taskReviewId}/{orderNum}/{oppNum}")
    @Produces(MediaType.TEXT_PLAIN)
	public String saveSummaryInfo(@PathParam("taskReviewId") String taskReviewId,@PathParam("orderNum") String orderNum,@PathParam("oppNum") String oppNum);
    
    @GET
    @Path("/update-state-info/{taskReviewId}/{taskReviewState}")
    @Produces(MediaType.TEXT_PLAIN)
	public String updateStateInfo(@PathParam("taskReviewId") String taskReviewId, @PathParam("taskReviewState") String taskReviewState);
    
    @GET
    @Path("/no-follow-info/{taskReviewId}/{orgId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String NoFollowUpInfo(@PathParam("taskReviewId") String taskReviewId, @PathParam("orgId") String orgId);
    
    @GET
    @Path("/home-card-list/")
    @Produces(MediaType.TEXT_PLAIN)
	public String findHomeCardRecords();
}
