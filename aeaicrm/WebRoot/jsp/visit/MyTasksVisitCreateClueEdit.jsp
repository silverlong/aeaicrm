<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>生成线索</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
    <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'createClue'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
    <aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>   
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>商机名称</th>
	<td><input id="OPP_NAME" label="商机名称" name="OPP_NAME" type="text" value="<%=pageBean.inputValue("OPP_NAME")%>" size="24" style="width:302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>客户名称</th>
	<td><input id="VISIT_CUST_ID_NAME" label="客户名称" name="VISIT_CUST_ID_NAME" type="text" value="<%=pageBean.inputValue("VISIT_CUST_ID_NAME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />
	<input id="CUST_ID" label="客户ID"  type="hidden" name="CUST_ID" value="<%=pageBean.inputValue("CUST_ID")%>" size="24" class="text" /></td>
</tr>
<!-- <tr> -->
<!-- 	<th width="100" nowrap>商机来源</th> -->
<%-- 	<td><select id="CLUE_SOURCE" label="来源方式" name="CLUE_SOURCE" class="select"><%=pageBean.selectValue("CLUE_SOURCE")%></select> --%>
<!-- </td> -->
<!-- </tr> -->
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="OPP_STATE_TEXT" label="状态" name="OPP_STATE_TEXT" type="text" value="<%=pageBean.selectedText("OPP_STATE")%>" size="24" style="width:302px" class="text" readonly="readonly"/>
	<input id="OPP_STATE" label="状态" name="OPP_STATE" type="hidden" value="<%=pageBean.selectedValue("OPP_STATE")%>" />
   </td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="OPP_CREATER_NAME" type="text" class="text"	id="OPP_CREATER_NAME"	value="<%=pageBean.inputValue("OPP_CREATER_NAME")%>" size="24" style="width:302px" readonly="readonly" label="创建人" /> 
		<input id="OPP_CREATER" label="创建人"  name="OPP_CREATER" type="hidden"	value="<%=pageBean.inputValue("OPP_CREATER")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="OPP_CREATE_TIME" label="创建时间" name="OPP_CREATE_TIME" type="text" value="<%=pageBean.inputTime("OPP_CREATE_TIME")%>" readonly="readonly" size="24" style="width:302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>级别</th>
	<td>
    <select id="OPP_LEVEL" label="级别" name="OPP_LEVEL" class="select"><%=pageBean.selectValue("OPP_LEVEL")%></select>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
<script language="javascript">
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
