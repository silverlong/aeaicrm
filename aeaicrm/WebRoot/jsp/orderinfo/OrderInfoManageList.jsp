<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>订单管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function controlUpdateBtn(stateResult){
	if(stateResult =='0'){
		enableButton("editImgBtn");
		disableButton("confirmImgBtn");
		enableButton("delImgBtn");
		enableButton("submitImgBtn");
	}
	if(stateResult =='1'){
		disableButton("editImgBtn");
		enableButton("confirmImgBtn");
		disableButton("delImgBtn");
		disableButton("submitImgBtn");
	}
	if(stateResult =='2'){
		disableButton("editImgBtn");
		disableButton("confirmImgBtn");
		disableButton("delImgBtn");
		disableButton("submitImgBtn");
	}
}
function goToBackList(){
	parent.closeBox();
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolBar" border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input id="editImgBtn" value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td></aeai:previlege>
   <aeai:previlege code="detail"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input id="detailImgBtn" value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td></aeai:previlege>
   <aeai:previlege code="copy"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td></aeai:previlege>
   <aeai:previlege code="submit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('doSubmit')"><input id="submitImgBtn" value="&nbsp;" title="提交" type="button" class="submitImgBtn" />提交</td></aeai:previlege>
   <aeai:previlege code="confirm"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('doConfirm')"><input id="confirmImgBtn" value="&nbsp;" title="确认" type="button" class="confirmImgBtn" />确认</td></aeai:previlege>   
   <aeai:previlege code="delete"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td></aeai:previlege>
   <aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBackList();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td></aeai:previlege>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;状态<select id="ORDER_STATE" label="状态" name="ORDER_STATE" class="select" onchange="doQuery()"><%=pageBean.selectValue("ORDER_STATE")%></select>
&nbsp;商机名称<input id="OPP_NAME" label="商机名称" name="OPP_NAME" type="text" value="<%=pageBean.inputValue("OPP_NAME")%>" size="18" class="text"  ondblclick="emptyText('OPP_NAME')" />
&nbsp;客户名称<input id="ORDER_NAME" label="客户名称" name="ORDER_NAME" type="text" value="<%=pageBean.inputValue("ORDER_NAME")%>" size="18" class="text" ondblclick="emptyText('ORDER_NAME')" />
&nbsp;跟进人员<input id="salesMan" label="跟进人员" name="salesMan" type="text" value="<%=pageBean.inputValue("salesMan")%>" size="18" class="text" ondblclick="emptyText('salesMan')" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" 
retrieveRowsCallback="process" 
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?15:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{ORDER_ID:'${row.ORDER_ID}'});controlUpdateBtn('${row.ORDER_STATE}');refreshConextmenu()" onclick="selectRow(this,{ORDER_ID:'${row.ORDER_ID}'});controlUpdateBtn('${row.ORDER_STATE}');">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="OPP_ID_NAME" title="商机名称"   />
    <ec:column width="100" property="ORDER_NAME" title="客户名称"   />
	<ec:column width="100" property="ORDER_CHIEF" title="负责人"   />
	<ec:column width="100" property="ORDER_COST" title="订单费用"   />
	<ec:column width="100" property="ORDER_STATE" title="状态"  mappingItem="ORDER_STATE" />
	<ec:column width="100" property="CLUE_SALESMAN_NAME" title="跟进人"   />
	<ec:column width="100" property="ORDER_CREATER_NAME" title="创建人"   />
	<ec:column width="100" property="ORDER_CREATE_TIME" title="创建时间" cell="date" format="yyyy-MM-dd" />
</ec:row>
</ec:table>
<input type="hidden" name="ORDER_ID" id="ORDER_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="custId" id="custId" value="<%=pageBean.inputValue("custId")%>" />
<script language="JavaScript">
setRsIdTag('ORDER_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
