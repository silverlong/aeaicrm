<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request"
	class="com.agileai.hotweb.domain.PageBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户导入</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link href="uploader/uploadify.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
document.write("<script type='text/javascript' " +"src='uploader/jquery.uploadify.js?" + new Date() + "'></s" + "cript>");

function closeCurrentRecord(){
	parent.PopupBox.closeCurrent();
}
function doDuplicateCheck(){
	doSubmit({actionType:'duplicateCheck'});
}
function doCreatData(){
	var url = "<%=pageBean.getHandlerURL()%>&actionType=creatData";
	sendRequest(url,{onComplete:function(responseText){
		if(responseText == "success"){
			jAlert("导入成功");
			parent.refresh();
		}
	}});
	
}
</script>
</head>
<body>
	<div id="__ToolBar__">
		<table border="0" cellpadding="0" cellspacing="1">
			<tr>
				<td onmouseover="onMover(this);" onmouseout="onMout(this);"
					class="bartdx" align="center" id="uploadify"><input
					type="button" class="createImgBtn" name="uploadify" /></td>
				<td onmouseover="onMover(this);" onmouseout="onMout(this);"
					class="bartdx" align="center" onclick="doSequenceUpload()"><input
					value="&nbsp;" title="导入" type="button" class="excelImgBtn"
					id="import" />导入</td>
				<td onmouseover="onMover(this);" onmouseout="onMout(this);"
					class="bartdx" align="center" onclick="downloadTemplate()"><input
					value="&nbsp;" type="button" class="downImgBtn" title="模板下载" />模板下载</td>
				<td onmouseover="onMover(this);" onmouseout="onMout(this);"
					class="bartdx" align="center" onclick="doDuplicateCheck()"><input
					value="&nbsp;" type="button" class="checkImgBtn" title="查重" />查重</td>
				<%if(pageBean.getBoolValue("doCreatData")){%>
				<td onmouseover="onMover(this);" onmouseout="onMout(this);"
					class="bartdx" align="center" onclick="doCreatData()"><input
					value="&nbsp;" type="button" class="creatDataImgBtn" title="加载" />加载</td>
				<%} %>									
				<td onmouseover="onMover(this);" onmouseout="onMout(this);"
					class="bartdx" align="center" onclick="closeCurrentRecord()"><input
					value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
			</tr>
		</table>
	</div>
	<div id="fileQueue"></div>	
	<input type="hidden" id="location" name="location" value="<%=pageBean.getAttribute("location")%>" />
	<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
		<input type="hidden" name="actionType" id="actionType" value="" />
		<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>" />
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" 
retrieveRowsCallback="process" 
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?1000:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();" oncontextmenu="selectRow(this,{ORG_ID:'${row.IMPORT_TEMP_ID}'});refreshConextmenu()" onclick="selectRow(this,{ORG_ID:'${row.IMPORT_TEMP_ID}'});">
<%-- 	<ec:column width="25" style="text-align:center" property="ORG_ID" cell="checkbox" headerCell="checkbox" onclick="selectedCheck('${GLOBALROWCOUNT}');"/> --%>
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="120" property="IMPORT_TEMP_NAME" title="客户名称" />
	<ec:column width="80" property="IMPORT_TEMP_DESC" title="公司概况"/>
	<ec:column width="100" property="IMPORT_TEMP_LINKMAN" title="联系人" />
	<ec:column width="50" property="IMPORT_TEMP_CONTACT_ONE" title="联系方式一" />
	<ec:column width="50" property="IMPORT_TEMP_CONTACT_TWO" title="联系方式二" />
</ec:row>
</ec:table>		
	</form>
	<script type="text/javascript">
var batchUploadCount = 0;
var sucessCount = 0;
$(document).ready(function() {
	$("#uploadify").uploadify({
		'height':18,
		'swf':'<%=request.getContextPath()%>/uploader/uploadify.swf',
		'uploader':'<%=request.getContextPath()%>/index;jsessionid=<%=session.getId()%>?ProcustUpload&actionType=uploadFile',
		'width':60,
		'fileTypeDesc':'Excel 文档',
		'fileTypeExts':'*.xls; *.xlsx;',
		'auto':false,
		'queueID':'fileQueue',
		'queueSizeLimit':5,
		'removeTimeout':1,
		'method':'get',
		'multi':true,
		'simUploadLimit':5,
		'multi':false,
		'formData':{'BIZ_ID':'<%=pageBean.inputValue("BIZ_ID")%>'},
		'sizeLimit':'<%=pageBean.inputValue("GRP_RES_SIZE_LIMIT")%>',
		'buttonClass':'ui-fileupload-buttonbar',
		'buttonText':'选择文件',	
		'overrideEvents' : ['onSelectError','onDialogClose' ],
        'onSelect': function (file) {
        	
        },
        'onSelectError': function (file, errorCode, errorMsg) {  
            switch (errorCode) {  
                case -100:  
                	jAlert("上传的文件数量已经超出系统限制的" + jQuery('#file_upload').uploadify('settings', 'queueSizeLimit') + "个文件！");  
                    break;  
                case -110:  
                	jAlert("文件 [" + file.name + "] 大小超出系统限制的" + jQuery('#uploadify').uploadify('settings', 'sizeLimit') + "大小！");  
                    break;  
                case -120:  
                	jAlert("文件 [" + file.name + "] 大小异常！");  
                    break;  
                case -130:  
                	jAlert("文件 [" + file.name + "] 类型不正确！");  
                    break;  
            }  
	      },
        'onDialogClose':function(event,queueId,fileObj,response,data){
        	//
        },	      
		'onUploadSuccess': function(fileObj, response, data) {
			doSubmit({actionType : 'prepareDisplay'});
			//当单个文件上传完成后触发
            //event:事件对象(the event object)
            //ID:该文件在文件队列中的唯一表示
            //fileObj:选中文件的对象，他包含的属性列表
            //response:服务器端返回的Response文本，我这里返回的是处理过的文件名称
            //data：文件队列详细信息和文件上传的一般数据
           // jAlert("文件:" + fileObj.name + " 上传成功！");
			//jAlert("response is " + response);
			//$('#uploadify').uploadify('upload');
        },
    	'onUploadError': function(file,errorCode,errorMsg,errorString,swfuploadifyQueue) {  
            //jAlert(errorString);
        },
        'onUploadStart': function (file) {
        	  //jAlert("文件:" + file.name + " 上传开始！");
        	//$('#uploadify').uploadify('settings','formData',{'SO_ID':$("#SO_ID").val(),'RIR_TEMPLATE_TYPE':$("#RIR_TEMPLATE_TYPE").val(),'RIR_YEAR':$("#RIR_YEAR").val(),'RIR_PERIOD':$("#RIR_PERIOD").val()});
        },
        'onQueueComplete':function(queueData){
			if (queueData.uploadsSuccessful >= batchUploadCount){
				doSubmit({actionType:'prepareDisplay',doValidate:'false'});
        	}
        },    
        'onError': function(event, queueID, fileObj) {//当单个文件上传出错时触发
        	jAlert("文件:" + fileObj.name + " 上传失败！");
        }
	});
});

function doSequenceUpload() {
	batchUploadCount = $(".uploadify-queue-item").size();
	$('#uploadify').uploadify('upload');
}

function downloadTemplate() {
	doSubmit({actionType : 'downloadTemplate'});
	hideSplash();
}
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
